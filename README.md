# Cahoot
This repo contains:

0. Scripts for listing Canvas details about your account.

1. Canvas + ~Kahoot~ = Cahoot.

## Functionality
The two main tools run at the command line:

`src/cahoot.py` takes a text file containing a question and answers,
creates a canvas quiz, set a number of minutes to unlock it for,
and then displays the results.

## Installation
Clone this repository.
In the root of the repository, run:
`pip3 install . --user`

This is optional, as you can run directly from source.
But, it's easier to run from a separate directory for each class,
when you install using pip as above.

## Configuration
The `./.config/*` directory is in `./.gitignore`,
so you don't push your own configs to the remote server.

The script relies on two configuration files:

### configs.toml
`./.config/configs.toml`

```toml
BASE_URI = "https://umsystem.instructure.com/api/v1"
COURSE_ID = "207352"
ASSIGNMENT_GROUP_ID = "408614"
```

`BASE_URI` token should be the url of the API of your Canvas instance.

`COURSE_ID` should use the course ID of your desired Canvas course.

`ASSIGNMENT_GROUP_ID` should be the assignment group you want your quizzes in.

If you had your own course, you could get these ID numbers by running the list script below.
The numbers above correspond to a Canvas sandbox course:

### api_key.gpg
`./.config/api_key.gpg`

Which should contain your Canvas API access token, encrypted with `gpg2`.
You can obtain an API access token in the Canvas web interface settings for your account.
Instructions for encrypting it are in `./src/get_configs.py`

## Running

### After `pip install .`
Where `./.config/` exists, 
and `example_inputs/cahoot_multiple_choice_question.toml` is a question you have copied from the templates and modified.

```
cahoot example_inputs/cahoot_multiple_choice_question.toml
```

### Directly from source
To list course and quiz data:
`python3 ./src/canvas_list_stuff.py`

To delete all quizzes in a course:
`python3 ./src/canvas_list_stuff.py`

To create and launch a quiz:
`python3 ./src/cahoot.py <cahoot_question_nn.toml>`

For example:
`python3 ./src/cahoot.py ./example_inputs/cahoot_multiple_choice.toml`

## Development and changes
For documentation of the Canvas API, see:

https://canvas.instructure.com/doc/api/all_resources.html

Please: https://en.wikipedia.org/wiki/KISS_principle

And run:
`./pre_commit.sh`

### Tasks
* [ ] Randomly choose between music in dir

* [ ] Create "question groups" within a quiz, so each person gets one of n quizzes, which when mathy, can be auto-generated.

* [x] Give some percentage credit to wrong answers (validate that the only thing needed is to edit the weight in the question)? This may not be possible?
    * [x] This can be done with multiple-answer questions: Just have one answer be "I was here".

* [x] `cahoot.py` should also display results:

* [-] Can we do second-level time resolution? 
There is a minor problem with minute-level time resolution.
With time as HH:MM:SS, if a quiz should be open for 2 minutes, 
and is released at HH:MM:30, then HH:MM+2:00 is only 00:01:30 long.
This means that the quiz is either too long, or short, randomly.

* [x] Create questions.
    * [x] The quizzes get opened, but are they visible/open?
* [x] The creator still needs to add questions to each quiz.
    * [x] Using the quiz id returned during new quiz creation, then create new questions.
* [x] This is a CLI-only terminal application, so it's just text, but some making it pretty are welcome.
* [x] The quiz creator should take a text file input, where each text file is one Cahoot question. The format should be a TOML file, directly congruent with the json structure representing the Canvas question type itself, but containing only the needed fields.
    * [x] The formats for each of the possible types can be explored by logging their json returned data, and exporting it as JSON, YAML, TOML, see `./example_structures/`
    * [-] Make and test templates for each question type in `./example_inputs/`
        * [x] Made one for multiple choice, and various others. Some are not worth it.
        * [x] Make and test little template examples for other question types, by manually trimming the extras from `./example_structures/`.
        * [-] Display functionality needs to be able to handle other question types too!

## To test and develop this
At MST, for those with permissions, instructors and GTAs can use:
https://canvasrequest.umsystem.edu/
to create a new sandbox course, to experiment with.

You will need to make a Canvas access token, in the web interface.

## Canvas has some API bugs
https://community.canvaslms.com/t5/Canvas-Developers-Group/Canvas-API-Structure-of-answer-object-for-numerical-answer/m-p/165776#M4573
The json returned for a given quiz question created in the web interface,
does not correspond to the json required to create one via the API...

In firefox, just before creating a question,
f12 (developer tools) -> network -> click on the post corresponding to the question
-> xhr -> look at the fields there...
