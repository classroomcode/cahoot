"""
Assumes you have your secret in a gunpg2 encrypted file:
"./config/api_key.gpg"
which started as plain text,
contains only the key/secret,
and is encrypted with `gpg2`.

To set this up:
cd ./config/
sudo dnf install gpgme gpgme-devel python3-gpg gnupg2
gpg2 --full-generate-key # If you don't already have a gpg2 key.
echo "yourkey" >api_key
gpg2 --encrypt --output api_key.gpg --recipient yourkeyid/email api_key
rm api_key
"""

import gpg  # type: ignore
import toml

# import yaml
# import json


def _decrypt_secret(filename: str) -> str:
    with open(filename, "rb") as ciphertext_file:
        try:
            key, result, verify_result = gpg.Context().decrypt(ciphertext_file)
            key = key.decode().strip()
        except gpg.errors.GPGMEError as e:
            key = None
            print(e)
    return str(key)


def load_config(key_filename: str, course_filename: str) -> tuple[str, str, str, str]:
    API_KEY = _decrypt_secret(filename=key_filename)
    with open(course_filename, "r") as file_handler:
        # Just for example of other options:
        # COURSE_CONFIGS = json.load(file_handler)
        # COURSE_CONFIGS = yaml.safe_load(file_handler)
        COURSE_CONFIGS = toml.load(file_handler)
    return (
        API_KEY,
        str(COURSE_CONFIGS["BASE_URI"]),
        str(COURSE_CONFIGS["COURSE_ID"]),
        str(COURSE_CONFIGS["ASSIGNMENT_GROUP_ID"]),
    )


def input_enforcer(possible_choices: list[int]) -> int:
    selection = 0
    while selection not in possible_choices:
        try:
            selection = int(input("Choose a number in the available range or set: "))
        except Exception as e:
            print(f"Exception was: {e}")
    return selection
