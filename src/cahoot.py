#!/usr/bin/python3
# -*- coding: utf-8 -*-
import audioplayer
import datetime
import json
import pathlib
import requests
import sys
import time
import toml
import utils

"""
pip3 install audioplayer --user
"""
import importlib_resources

my_resources = importlib_resources.files("cahoot")
music_path = str(my_resources.joinpath("music", "Kahoot_Music.mp3"))

clear_screen = "\033[H\033[J"


def load_quiz_file(filename: str) -> dict[str, str]:
    with open(filename) as file_handler:
        question = toml.load(file_handler)
    # TODO Maybe later, add standard repeat fields here,
    # rather than repeatedly write them in each question file?
    return question


def create_quiz(
    api_token: str, base_uri: str, course_id: str, assignment_group_id: str, title: str
) -> int:
    url = f"{base_uri}/courses/{course_id}/quizzes"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = {
        "quiz": {
            "title": title,
            "assignment_group_id": assignment_group_id,
            "shuffle_answers": True,
            "points_possible": 1,
        }
    }
    response = requests.post(url, headers=headers, json=payload)
    # print(json.dumps(response.json(), indent=4))
    return int(response.json()["id"])


def create_question(
    api_token: str,
    base_uri: str,
    course_id: str,
    quiz_id: int,
    quiz_data: dict[str, str],
) -> None:
    url = f"{base_uri}/courses/{course_id}/quizzes/{quiz_id}/questions"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = quiz_data
    response = requests.post(url, headers=headers, json=payload)
    # print(json.dumps(response.json(), indent=4))


def open_quiz(
    api_token: str, base_uri: str, course_id: str, quiz_id: int, target_time: int
) -> None:
    url = f"{base_uri}/courses/{course_id}/quizzes/{quiz_id}"
    headers = {"Authorization": f"Bearer {api_token}"}
    now = datetime.datetime.now(datetime.UTC)
    unlock_at = now.isoformat() + "Z"
    lock_at = (now + datetime.timedelta(minutes=target_time)).isoformat() + "Z"
    payload = {
        "quiz": {
            "unlock_at": unlock_at,
            "lock_at": lock_at,
            "due_at": lock_at,
            "published": True,
        }
    }
    response = requests.put(url, headers=headers, json=payload)
    # print(json.dumps(response.json(), indent=4), response.status_code)


def get_quiz_results(
    api_token: str, base_uri: str, course_id: str, quiz_id: int
) -> dict[str, str | int]:
    url = f"{base_uri}/courses/{course_id}/quizzes/{quiz_id}/statistics"
    headers = {"Authorization": f"Bearer {api_token}"}
    response = requests.get(url, headers=headers)
    # print(json.dumps(response.json(), indent=4), response.status_code)
    if len(response.json()["quiz_statistics"][0]["question_statistics"]) == 0:
        return {}
    else:
        question_stats = response.json()["quiz_statistics"][0]["question_statistics"][0]
        return question_stats


def main() -> None:
    api_token, base_uri, course_id, assignment_group_id = utils.load_config(
        key_filename="./.config/api_key.gpg", course_filename="./.config/configs.toml"
    )
    if not (1 < len(sys.argv) < 3):
        print("List one cahoot file as first argument")
        sys.exit(1)
    quiz_name = sys.argv[1]
    title = pathlib.Path(quiz_name).stem
    quiz_id = create_quiz(api_token, base_uri, course_id, assignment_group_id, title)
    quiz_data = load_quiz_file(quiz_name)
    print(clear_screen)
    print(f"Launching {title}:\n")
    print("-------------------------")
    print(f"{quiz_data['question']['the_actual_question']}\n")
    for answer in quiz_data["question"]["answers"]:
        print(f"\t{answer['text']}\n")
    create_question(api_token, base_uri, course_id, quiz_id, quiz_data)
    durations = list(range(1, 5))
    print("-------------------------")
    print(f"Minutes quiz can be open: {durations}")
    target_time = utils.input_enforcer(durations)
    open_quiz(api_token, base_uri, course_id, quiz_id, target_time)
    player = audioplayer.AudioPlayer(music_path)
    print("\nQuiz is now open, waiting for results to display...")
    player.play(loop=True)
    time.sleep(target_time * 60 + 10)
    player.stop()
    results = get_quiz_results(api_token, base_uri, course_id, quiz_id)
    print(clear_screen)
    if results == {}:
        print("No one answered the quiz.")
    else:
        print(f'{title}\n\n{quiz_data["question"]["the_actual_question"]}\n')
        for i in range(len(results["answers"])):
            percent = (results["answers"][i]["responses"] / results["responses"]) * 100
            proportion = f"{results['answers'][i]['responses']}/{results['responses']}"
            question = f"{results['answers'][i]['text']}"
            bar = ("=" * round(percent / 5)).ljust(20, " ")
            if results["answers"][i]["correct"]:
                print(f"~ |{bar} {percent}%\t({proportion})\t{question}\n")
            else:
                print(f"  |{bar} {percent}%\t({proportion})\t{question}\n")


if __name__ == "__main__":
    main()
