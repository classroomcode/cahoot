import json
import requests
import toml
import utils
import yaml

response_json = list[dict[str, str | int]]


def get_courses(api_token: str, base_uri: str) -> response_json:
    url = f"{base_uri}/courses"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = {"page": 1, "per_page": 1000}
    response = requests.get(url, headers=headers, json=payload)
    # print(response.status_code)
    return response.json()


def get_students(api_token: str, base_uri: str, course_id: str) -> response_json:
    url = f"{base_uri}/courses/{course_id}/students"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = {"page": 1, "per_page": 1000}
    response = requests.get(url, headers=headers, json=payload)
    # response.status_code)
    return response.json()


def get_assignment_groups(
    api_token: str, base_uri: str, course_id: str
) -> response_json:
    url = f"{base_uri}/courses/{course_id}/assignment_groups"
    headers = {"Authorization": f"Bearer {api_token}"}
    response = requests.get(url, headers=headers)
    # print(response.status_code)
    return response.json()


def get_quizzes(api_token: str, base_uri: str, course_id: str) -> response_json:
    url = f"{base_uri}/courses/{course_id}/quizzes"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = {"page": 1, "per_page": 1000}
    response = requests.get(url, headers=headers, json=payload)
    # print(response.status_code)
    return response.json()


def delete_quiz(api_token: str, base_uri: str, course_id: str, quiz_id: str) -> None:
    url = f"{base_uri}/courses/{course_id}/quizzes/{quiz_id}"
    headers = {"Authorization": f"Bearer {api_token}"}
    response = requests.delete(url, headers=headers)
    # print(json.dumps(response.json(), indent=4), response.status_code)


def get_assignments(api_token: str, base_uri: str, course_id: str) -> response_json:
    url = f"{base_uri}/courses/{course_id}/assignments"
    headers = {"Authorization": f"Bearer {api_token}"}
    payload = {"page": 1, "per_page": 1000}
    response = requests.get(url, headers=headers, json=payload)
    # print(response.status_code)
    return response.json()


def get_questions(
    api_token: str, base_uri: str, course_id: str, quiz_id: str
) -> response_json:
    url = f"{base_uri}/courses/{course_id}/quizzes/{quiz_id}/questions"
    headers = {"Authorization": f"Bearer {api_token}"}
    response = requests.get(url, headers=headers)
    # print(response.status_code)
    return response.json()
