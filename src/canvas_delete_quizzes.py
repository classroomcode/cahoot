#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This is for deleting all Canvas quiz content.
Execute it carefully!
"""
import json
import requests
import toml
import utils
import yaml
import canvas_funcs


def main() -> None:
    api_token, base_uri, course_id, assignment_group_id = utils.load_config(
        key_filename="./.config/api_key.gpg", course_filename="./.config/configs.toml"
    )

    courses = canvas_funcs.get_courses(api_token, base_uri)
    print("Course example:")
    print(json.dumps(courses[0], indent=4))
    print("Course summary:")
    print(
        json.dumps(
            {obj["name"]: obj["id"] for obj in courses if "name" in obj.keys()},
            indent=4,
        )
    )

    course_id = input("Which course ID do you want? ")

    for quiz in canvas_funcs.get_quizzes(api_token, base_uri, course_id):
        canvas_funcs.delete_quiz(api_token, base_uri, course_id, quiz["id"])


if __name__ == "__main__":
    main()
