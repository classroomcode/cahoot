#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""This is just for listing Canvas content, to help you assign IDs, etc."""
import json
import requests
import toml
import utils
import yaml
import canvas_funcs


def main() -> None:
    api_token, base_uri, course_id, assignment_group_id = utils.load_config(
        key_filename="./.config/api_key.gpg", course_filename="./.config/configs.toml"
    )

    courses = canvas_funcs.get_courses(api_token, base_uri)
    print("Course example:")
    print(json.dumps(courses[0], indent=4))
    print("Course summary:")
    print(
        json.dumps(
            {obj["name"]: obj["id"] for obj in courses if "name" in obj.keys()},
            indent=4,
        )
    )

    course_id = input("Which course ID do you want? ")

    students = canvas_funcs.get_students(api_token, base_uri, course_id)
    print(f"\nStudents for course ID: {course_id}: (1/{len(students)}):")
    print(json.dumps(students[0], indent=4))

    assignment_groups = canvas_funcs.get_assignment_groups(
        api_token, base_uri, course_id
    )
    print(f"\nAssignment groups for course ID: {course_id}")
    print(json.dumps(assignment_groups, indent=4))

    quizzes = canvas_funcs.get_quizzes(api_token, base_uri, course_id)
    print(f"\nQuizzes for course ID: {course_id}: (1/{len(quizzes)}):")
    print(json.dumps(quizzes[0], indent=4))

    assignments = canvas_funcs.get_assignments(api_token, base_uri, course_id)
    print(f"\nAssignments for course ID: {course_id}: (1/{len(assignments)}):")
    print(json.dumps(assignments[0], indent=4))

    # I made an example quiz in canvas with all the question types I wanted:
    # quiz_id = 622802
    # questions = canvas_funcs.get_questions(api_token, base_uri, course_id, quiz_id)
    # for question in questions:
    #     with open(
    #         f"./example_structures/{question['question_type']}.json", "w"
    #     ) as fhand:
    #         json.dump(question, fhand, indent=4)
    #     with open(
    #         f"./example_structures/{question['question_type']}.toml", "w"
    #     ) as fhand:
    #         toml.dump(question, fhand)
    #     with open(
    #         f"./example_structures/{question['question_type']}.yaml", "w"
    #     ) as fhand:
    #         yaml.dump(question, fhand)


if __name__ == "__main__":
    main()
